	INTRODUCTION
	============

 Audit monitoring is a module that coupled with the monitoring module, facilitates
 the audit of a site through different sensors that will go check 
 some control points. 
 The report of these control points is then accessible via the interface 
 (admin / reports / monitoring). The controls are listed according to different 
 categories (Performance, Security, System ...) 
 and different statuses (OK, WARNING, CRITICAL).

	REQUIREMENTS
	============

* PHP min version : 5.3
* Dependencies : monitoring:8.x-1.7
* Patch : monitoring-improved_ConfigValueSensorPlugin-3080150-#2.patch 

	INSTALLATION
	============

* Use composer : composer require drupal/audit_monitoring:1.x-dev

	CONFIGURATION
	=============

 Depending on the environment of the site you want to audit, you can override some
 configurations in settings.php.

* For production environment :

 $config['monitoring.sensor_config.node_new_all']['status'] = true;
 $config['monitoring.sensor_config.core_theme_default']['status'] = true;
 $config['monitoring.sensor_config.database_disk_usage']['status'] = true;
 $config['monitoring.sensor_config.system_memory']['status'] = true;
 $config['monitoring.sensor_config.user_active']['status'] = true;

* For pre-production environment :

 $config['monitoring.sensor_config.node_new_all']['status'] = true;
 $config['monitoring.sensor_config.core_theme_default']['status'] = true;
 $config['monitoring.sensor_config.database_disk_usage']['status'] = true;
 $config['monitoring.sensor_config.system_memory']['status'] = true;
 $config['monitoring.sensor_config.user_active']['status'] = true;
 $config['monitoring.sensor_config.maillog_configuration']['status'] = true;

* For development environment :

 $config['monitoring.sensor_config.core_theme_default']['status'] = true;
 $config['monitoring.sensor_config.monitoring_git_dirty_tree']['status'] = true;
 $config['monitoring.sensor_config.enabled_modules_dev']['status'] = true;
 $config['monitoring.sensor_config.enabled_modules']['status'] = false;
 $config['monitoring.sensor_config.user_new']['status'] = false;
 $config['monitoring.sensor_config.comment_new']['status'] = false;
 $config['monitoring.sensor_config.maillog_records_count']['status'] = false;
 $config['monitoring.sensor_config.node_new_all']['status'] = false;
 $config['monitoring.sensor_config.database_disk_usage']['status'] = true;
 $config['monitoring.sensor_config.system_memory']['status'] = true;

